#!/bin/bash

# A simple example script that publishes a number of scripts to the Nexus Repository Manager
# and executes them.

# fail if anything errors
set -e
# fail if a function call is missing an argument
set -u

username=admin
password="$(cat /nexus-data/admin.password)"
groovyexec=$NEXUS_HOME/system/org/codehaus/groovy/groovy-all/2.4.15/groovy-all-2.4.15.jar

# add the context if you are not using the root context
host=http://localhost:8081

# add a script to the repository manager and run it
function addAndRunScript {
  name=$1
  file=$2
  java -jar $groovyexec -v -Dgroovy.grape.report.downloads=true -Dgrape.config=grapeConfig.xml deploy-config/nexus/addUpdateScript.groovy -u "$username" -p "$password" -n "$name" -f "$file" -h "$host"
  printf "\nPublished $file as $name\n\n"
  curl -v -X POST -u $username:$password --header "Content-Type: text/plain" "$host/service/rest/v1/script/$name/run"
  printf "\nSuccessfully executed $name script\n\n\n"
}

printf "Provisioning Integration API Scripts Starting \n\n" 
printf "Publishing and executing on $host\n"

addAndRunScript security deploy-config/nexus/security.groovy

printf "\nProvisioning Scripts Completed\n\n"
