import jenkins.model.*

def inst = Jenkins.getInstance()

def desc = inst.getDescriptor("hudson.plugins.git.GitSCM")

desc.setGlobalConfigName(System.getenv('PROJECT_REPOSITORY_USER'))
desc.setGlobalConfigEmail(System.getenv('PROJECT_REPOSITORY_EMAIL'))

desc.save()


