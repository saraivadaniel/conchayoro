String basePath = 'dsljobs'
String repo = System.getenv("PROJECT_REPOSITORY_HOST")

folder(basePath) {
  description 'DSL generated folder.'
}

pipelineJob('conchayoro-pipeline') {

  definition {

    cpsScm {

      scm {

        git {

          remote {

            url(repo)

          }

        }


      }

      scriptPath('Jenkinsfile')

    }

  }

  triggers {

      cron('@midnight')

  }

}
